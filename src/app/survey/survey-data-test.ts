export namespace TestSurvey {

    export const home = {
        'spot': [{ 'Column 1': 'home' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const outdoors = {
        'spot': [{ 'Column 1': 'outdoors' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const work = {
        'spot': [{ 'Column 1': 'work' }, { 'Column 1': 'supermarket' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const diner = {
        'spot': [{ 'Column 1': 'diner' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const mall = {
        'spot': [{ 'Column 1': 'mall' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const classroom = {
        'spot': [{ 'Column 1': 'classroom' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };

    export const supermarket = {
        'spot': [{ 'Column 1': 'supermarket' }],
        'transport': 'bus',
        'busy': 5,
        'dinner': 'order',
        'age': 80,
        'sickness': 3,
        'drink': 4
    };


}


