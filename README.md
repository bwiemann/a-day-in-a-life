# A Day In A Live

"A Day In A Live" is a web app, which enables the user to choose a day in his live and transform it into a piece of music. When visiting the web page https://bwiemann.gitlab.io/a-day-in-a-life , a couple of questions about the details of the day are asked. The answers of the questions are used to generate the piece of music. 

This project was realized by Sebastian Weber and Benjamin Wiemann. 